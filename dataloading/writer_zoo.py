import os.path
from .regex import ImageFolder
from dataloading.constants import get_dataset_config
class WriterZoo:
    @staticmethod
    def new(desc, **kwargs):
        return ImageFolder(desc['path'], regex=desc['regex'], **kwargs)

    @staticmethod
    def get(is_cluster, dataset, set, **kwargs, ):
        _all = get_dataset_config(is_cluster)

        d = _all[dataset]
        s = d['set'][set]

        s['path'] = os.path.join(d['basepath'], s['path'])
        return WriterZoo.new(s, **kwargs)

CLUSTER_PATH = "/home/woody/iwi5/iwi5157h/wi_static/"
MACHINE_PATH =  '/vol/ideadata/ir45ucej/wi/static'
ALEX_CLUSTER_PATH = "/home/atuin/b143dc/b143dc35/wi_static/"

datasets = {
    'icdar2017': {
        'basepath': MACHINE_PATH,
        'set': {
            'test': {'path': 'binary_test_2017',
                     'regex': {'writer': '(\d+)', 'page': '\d+-IMG_MAX_(\d+)'}},
            'train': {'path': 'binary_train_2017',
                      'regex': {'cluster': '(\d+)', 'writer': '\d+_(\d+)', 'page': '\d+_\d+-IMG_MAX_(\d+)'}},
        }
    },

    'icdar2013': {
        'basepath': MACHINE_PATH,
        'set': {
            'test': {'path': 'icdar2013_test_sift_patches_binarized',
                     'regex': {'writer': '(\d+)', 'page': '\d+_(\d+)'}},

            'train': {'path': 'icdar2013_train_sift_patches_1000/',
                      'regex': {'cluster': '(\d+)', 'writer': '\d+_(\d+)', 'page': '\d+_\d+_(\d+)'}}
        }
    },

    'icdar2019': {
        'basepath': MACHINE_PATH,
        'set': {
            'test': {'path': 'wi_comp_19_test_patches',
                     'regex': {'writer': '(\d+)', 'page': '\d+_(\d+)'}},

            'train': {'path': 'wi_comp_19_validation_patches',
                      'regex': {'cluster': '(\d+)', 'writer': '\d+_(\d+)', 'page': '\d+_\d+_(\d+)'}},
        }
    }
}


def get_dataset_config(is_cluster, is_alex =False):
    config = datasets.copy()
    if is_cluster:
        work_path = ALEX_CLUSTER_PATH if is_alex else CLUSTER_PATH
        config['icdar2017']['basepath'] = work_path
        config['icdar2013']['basepath'] = work_path
        config['icdar2019']['basepath'] = work_path

    else:
        config['icdar2017']['set']['test']['path'] = 'test_sample'
    return config



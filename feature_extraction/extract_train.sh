cd /vol/ideadata/ir45ucej/wi/wi_harendotes
conda activate wi_env
python -m helpers.extract_patches \
  --in_dir /vol/ideadata/ir45ucej/wi/static/icdar2017-training-binary \
  --out_dir /vol/ideadata/ir45ucej/wi/static/binary_train_2017
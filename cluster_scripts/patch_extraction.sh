#!/bin/bash -l
#SBATCH --job-name=train_patches
#SBATCH --ntasks=1
#SBATCH --gres=gpu:1
#SBATCH --output=R-%x.%j.out
#SBATCH --error=R-%x.%j.err
#SBATCH --mail-type=end,fail
#SBATCH --time=24:00:00
#SBATCH --export=NONE
unset SLURM_EXPORT_ENV

source ~/.bashrc

# Set proxy to access internet from the node

export http_proxy=http://proxy:80
export https_proxy=http://proxy:80

module purge
module load python
module load cuda
module load cudnn
echo "Going home"

cd ${WORK}/wi
# Conda

echo "Activate env"
source activate wi_cluster_env

# Run training script
echo "Run"
srun python -m helpers.extract_patches \
  --in_dir ${WORK}/wi_static/icdar2017-training-binary \
  --out_dir ${WORK}/wi_static/binary_train_2017 \
  --num_of_cluster 1000

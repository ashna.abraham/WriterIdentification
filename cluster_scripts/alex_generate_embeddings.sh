#!/bin/bash -l

#SBATCH --ntasks=1
#SBATCH --gres=gpu:a40:1
#SBATCH --output=R-%x.%j.out
#SBATCH --error=R-%x.%j.err
#SBATCH --mail-type=end,fail
#SBATCH --time=24:00:00
#SBATCH --export=NONE
unset SLURM_EXPORT_ENV

source ~/.bashrc

# Set proxy to access internet from the node

export http_proxy=http://proxy:80
export https_proxy=http://proxy:80

module purge
module load python
module load cuda
module load cudnn
echo "Going home"

cd ${WORK}/wi
# Conda

echo "Activate env"
source activate wi_cluster_env

# Run training script
echo "Run"
srun python -m rejection.embeddings.generate_embeddings --is_cluster --is_alex

root = '/vol/ideadata/ir45ucej/wi/static'
model_path = f'{root}/models/model.pt'
embeddings_path = f'{root}/embeddings_binary_test_2017'
# embeddings_path =f'{root}/test_emb'
train_embeddings_path = f'{root}/embeddings_binary_train_2017'
re_ranked_embeddings = f'{root}/ranked_emb_binary_test_2017/pages'
re_ranked_embeddings_full = f'{root}/ranked_emb_binary_test_2017/full'

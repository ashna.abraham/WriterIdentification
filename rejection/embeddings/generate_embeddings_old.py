import torch
from backbone.model import Model
from backbone import resnets
from dataloading.writer_zoo import WriterZoo
from torchvision import transforms
from tqdm import tqdm
import numpy as np
from utils.utils import load_config
from page_encodings import SumPooling
from sklearn.decomposition import PCA
from sklearn.preprocessing import normalize
import os
import torch.multiprocessing
torch.multiprocessing.set_sharing_strategy('file_system')
from dataloading.constants import get_dataset_config
import logging, argparse, re, glob
from pathlib import Path
from dataloading.GenericDataset import FilepathImageDataset
from dataloading.regex import pil_loader

def load_model(args):
    checkpoint = torch.load(constants.model_path)
    backbone = getattr(resnets, args['model']['name'], None)()

    random = args['model'].get('encoding', None) == 'netrvlad'

    model = Model(backbone, dim=64, num_clusters=args['model']['num_clusters'], random=random)
    model = model.cuda()
    model.load_state_dict(checkpoint['model_state_dict'])
    model.eval()
    return model

def get_page_features(model, args):
    testset = args['testset']
    is_cluster = args['is_cluster']
    dataset = get_dataset_config(is_cluster)
    ds = dataset[testset['dataset']]['set'][testset['set']]
    path = ds['path']
    regex = ds['regex']
    poolings = [SumPooling('l2', pn_alpha=0.4)]
    pfs_per_pooling = [[] for i in poolings]


    regex_w = regex.get('writer')
    regex_p = regex.get('page')

    srcs = sorted(list(glob.glob(f'{dataset[testset["dataset"]]["basepath"]}/{path}/**/*.png', recursive=True)))
    logging.info(f'Found {len(srcs)} images')
    writer = [int('_'.join(re.search(regex_w, Path(f).name).groups())) for f in srcs]
    page = [int('_'.join(re.search(regex_p, Path(f).name).groups())) for f in srcs]

    labels = list(zip(writer, page))
    transform = transforms.ToTensor()

    np_writer = np.array(writer)
    np_page = np.array(page)

    print(f'Found {len(list(set(labels)))} pages.')

    writers = []
    pages = []
    for w, p in tqdm(set(labels), 'Page Features'):
        idx = np.where((np_writer == w) & (np_page == p))[0]
        fps = [srcs[i] for i in idx]
        ds = FilepathImageDataset(fps, pil_loader, transform)
        loader = torch.utils.data.DataLoader(ds, num_workers=4, batch_size=args['test_batch_size'])

        feats = []
        for img in loader:
            img = img.cuda()

            with torch.no_grad():
                feat = model(img)
                feat = torch.nn.functional.normalize(feat)
            feats.append(feat.detach().cpu().numpy())

        feats = np.concatenate(feats)

        for i, pooling in enumerate(poolings):
            enc = pooling.encode([feats])
            pfs_per_pooling[i].append(enc)
        writers.append(w)
        pages.append(p)

    torch.cuda.empty_cache()
    pfs_per_pooling = [np.concatenate(pfs) for pfs in pfs_per_pooling]

    return pfs_per_pooling, writers, pages


def run_inference(model, logger, args):
    sumps, poolings = [], []
    sumps.append(SumPooling('l2', pn_alpha=0.4))
    poolings.append('SumPooling-PN0p4')

    # extract the global page descriptors
    pfs_per_pooling, writer, pages = get_page_features(model, args)

    for i, pfs in enumerate(pfs_per_pooling):
        for pca_dim in [512]:
            pca_dim = min(min(pfs.shape), pca_dim)
            print(f'Fitting PCA with shape {pca_dim}')
            pca = PCA(pca_dim, whiten=True)
            pfs_tf = pca.fit_transform(pfs)
            pfs_tf = normalize(pfs_tf, axis=1)



def load_data(args):
    val_ds = WriterZoo.get(is_cluster, **args['testset'])
    test_transform = transforms.ToTensor()
    val_ds = val_ds.TransformImages(transform=test_transform).SelectLabels(label_names=['writer', 'page'])
    val_ds = torch.utils.data.Subset(val_ds, range(len(val_ds)))
    # val_ds = torch.utils.data.Subset(val_ds, range(len(10)))

    batch_size = 256 #args['test_batch_size']
    loader = torch.utils.data.DataLoader(val_ds, num_workers=4,  batch_size=batch_size)
    return loader

def derive_patch_embeddings(data_loader, model):
    feats = []
    pages = []
    writers = []
    for sample, labels in tqdm(data_loader, desc='Inference'):
        w, p = labels[0], labels[1]
        writers.append(w)
        pages.append(p)
        sample = sample.cuda()
        with torch.no_grad():
            emb = model(sample)
            emb = torch.nn.functional.normalize(emb)
        feats.append(emb.detach().cpu().numpy())

    feats = np.concatenate(feats)
    writers = np.concatenate(writers)
    pages = np.concatenate(pages)
    return feats, writers, pages

def derive_page_embeddings(features, writers, pages):
    _labels = list(zip(writers, pages))

    labels_np = np.array(_labels)
    features_np = np.array(features)
    writer = labels_np[:, 0]
    page = labels_np[:, 1]

    page_features = []
    page_writer = []
    page_number = []
    for w, p in tqdm(set(_labels), 'Page Features'):
        idx = np.where((writer == w) & (page == p))
        page_features.append(features_np[idx])
        page_writer.append(w)
        page_number.append(p)

    return page_features, page_writer, page_number

def reduce_dimension(page_features):
    pca_dim = 512
    pca_dim = min(min(page_features.shape), pca_dim)
    pca = PCA(pca_dim, whiten=True)
    pg_tf = pca.fit_transform(page_features)
    pg_tf = normalize(pg_tf, axis=1)
    return pg_tf

def save_embeddings(embeddings, writers, pages):
    os.makedirs(constants.embeddings_path, exist_ok=True)
    for i, page in enumerate(pages):
        writer = writers[i]
        file_name = f'{constants.embeddings_path}/{writer}-IMG_MAX_{page}.npy'
        np.save(file_name, embeddings[i])




def main(args):
    model = load_model(args)
    data_loader = load_data(args)
    desc, writer, pages = derive_patch_embeddings(data_loader, model)
    page_features, page_writer, page_number = derive_page_embeddings(desc, writer, pages)
    # Aggregate all embeddings from a page using l2 norm and sum pooling
    norm = 'powernorm'
    pooling = SumPooling(norm)
    page_desc = pooling.encode(page_features)
    #PCA for dim reduction
    page_desc = reduce_dimension(page_desc)

    save_embeddings(page_desc,page_writer, page_number)




if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    parser.add_argument('--config', default='config/icdar2017.yml')
    parser.add_argument('--is_cluster', action='store_true', help='Running on cluster', default=False)

    args = parser.parse_args()
    is_cluster = args.is_cluster
    args.config = 'config/icdar2017.yml' if is_cluster else '../config/icdar2017.yml'

    if is_cluster:
        import rejection.constants.cluster_constants as constants
    else:
        import rejection.constants.gpu_constants as constants
    config = load_config(args)[0]

    main(config)

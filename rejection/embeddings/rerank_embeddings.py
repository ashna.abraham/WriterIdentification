import argparse
import glob
import logging
import numpy as np
from reranking import sgr
import torch
import os
def read_files():
    path = f'{constants.embeddings_path}/*.npy'
    files = sorted(glob.glob(path))
    logging.info(f'Found {len(files)} files')
    combined_data = []
    for file in files:
        data = np.load(file)
        combined_data.append(data)
    combined_data = np.array(combined_data)
    return combined_data, files

def save_embeddings(files, X, distance):
    os.makedirs(constants.re_ranked_embeddings_full, exist_ok=True)
    emb_file_path = f'{constants.re_ranked_embeddings_full}/desc.npy'
    dis_file_path = f'{constants.re_ranked_embeddings_full}/distance.npy'
    np.save(emb_file_path, X)
    np.save(dis_file_path, distance)

    os.makedirs(constants.re_ranked_embeddings, exist_ok=True)
    for i, file in enumerate(files):
        file_name = file.split('/')[-1]
        file_path = f'{constants.re_ranked_embeddings}/{file_name}'
        np.save(file_path, X[i])


def main():
    data, files = read_files()
    X = torch.tensor(data)
    X_reranked, distance = sgr.sgr_reranking(X, k=2, layer=1, gamma=0.4)
    save_embeddings(files, X_reranked, distance)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    parser.add_argument('--is_cluster', action='store_true', help='Running on cluster', default=False)
    parser.add_argument('--is_alex', action='store_true', help='Running on alex cluster', default=False)

    args = parser.parse_args()
    is_cluster = args.is_cluster
    is_alex = args.is_alex

    if is_cluster:
        if is_alex:
            import rejection.constants.alex_cluster_constants as constants
        else:
            import rejection.constants.cluster_constants as constants
    else:
        import rejection.constants.gpu_constants as constants

    main()




import torch
from backbone.model import Model
from backbone import resnets
from torchvision import transforms
from tqdm import tqdm
import numpy as np
from utils.utils import load_config
from page_encodings import SumPooling
from sklearn.decomposition import PCA
from sklearn.preprocessing import normalize
import os
import torch.multiprocessing
torch.multiprocessing.set_sharing_strategy('file_system')
from dataloading.constants import get_dataset_config
import logging, argparse, re, glob
from pathlib import Path
from dataloading.GenericDataset import FilepathImageDataset
from dataloading.regex import pil_loader

def load_model(args):
    checkpoint = torch.load(constants.model_path)
    backbone = getattr(resnets, args['model']['name'], None)()
    random = args['model'].get('encoding', None) == 'netrvlad'
    model = Model(backbone, dim=64, num_clusters=args['model']['num_clusters'], random=random)
    model.load_state_dict(checkpoint['model_state_dict'])
    model.eval()
    model = model.cuda()
    return model

def load_data(args):
    is_test = args['is_test']
    testset = args['testset'] if is_test else args['trainset']
    is_cluster = args['is_cluster']
    is_alex = args['is_alex']
    dataset = get_dataset_config(is_cluster, is_alex)
    ds = dataset[testset['dataset']]['set'][testset['set']]
    path = ds['path']
    regex = ds['regex']

    regex_w = regex.get('writer')
    regex_p = regex.get('page')

    srcs = sorted(list(glob.glob(f'{dataset[testset["dataset"]]["basepath"]}/{path}/**/*.png', recursive=True)))
    logging.info(f'Found {len(srcs)} images')
    writers = [int('_'.join(re.search(regex_w, Path(f).name).groups())) for f in srcs]
    pages = [int('_'.join(re.search(regex_p, Path(f).name).groups())) for f in srcs]
    print(f'Found {len(list(set(pages)))} pages.')
    return srcs, writers, pages

def derive_patch_embeddings(model, loader):
    feats = []
    for img in loader:
        img = img.cuda()
        with torch.no_grad():
            feat = model(img)
            feat = torch.nn.functional.normalize(feat) # l2 normalise
        feats.append(feat.detach().cpu().numpy())
    return feats


def derive_page_embeddings(model, srcs, writers, pages, args):
    labels = list(zip(writers, pages))
    transform = transforms.ToTensor()
    np_writer = np.array(writers)
    np_page = np.array(pages)
    pooling = SumPooling('powernorm', pn_alpha=0.4)
    page_embeddings = []
    page_writers = []
    page_numbers = []
    for w, p in tqdm(set(labels), 'Page Features'):
        indices = np.where((np_writer == w) & (np_page == p))[0]
        files = [srcs[i] for i in indices]
        ds = FilepathImageDataset(files, pil_loader, transform)
        loader = torch.utils.data.DataLoader(ds, num_workers=4, batch_size=args['test_batch_size'])
        page_features = derive_patch_embeddings(model, loader)
        page_features = np.concatenate(page_features)
        page_features = pooling.encode([page_features])
        page_embeddings.append(page_features)
        page_writers.append(w)
        page_numbers.append(p)
    torch.cuda.empty_cache()
    page_embeddings = np.concatenate(page_embeddings)
    return page_embeddings, np.array(page_writers), np.array(page_numbers)

def reduce_dimension(page_features):
    pca_dim = 512
    pca_dim = min(min(page_features.shape), pca_dim)
    page_features = normalize(page_features, axis=1)
    pca = PCA(pca_dim, whiten=True)
    pg_tf = pca.fit_transform(page_features)
    pg_tf = normalize(pg_tf, axis=1)
    return pg_tf

def save_embeddings(args, embeddings, writers, pages):
    is_test = args['is_test']
    save_path = constants.embeddings_path if is_test else constants.train_embeddings_path
    os.makedirs(save_path, exist_ok=True)
    for i, page in enumerate(pages):
        writer = writers[i]
        file_name = f'{save_path}/{writer}-IMG_MAX_{page}.npy'
        np.save(file_name, embeddings[i])

def main(args):
    model = load_model(args)
    srcs, writers, pages = load_data(args)
    page_features, page_writer, page_number = derive_page_embeddings(model, srcs, writers, pages, args)
    page_desc = reduce_dimension(page_features)
    save_embeddings(args, page_desc,page_writer, page_number)



if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    parser.add_argument('--config', default='config/icdar2017.yml')
    parser.add_argument('--is_cluster', action='store_true', help='Running on cluster', default=False)
    parser.add_argument('--is_alex', action='store_true', help='Running on alex cluster', default=False)
    parser.add_argument('--is_test', action='store_true', help='Test or train', default=False)

    args = parser.parse_args()
    is_cluster = args.is_cluster
    is_alex = args.is_alex

    args.config = 'config/icdar2017.yml' if is_cluster else '../config/icdar2017.yml'

    if is_cluster:
        if is_alex:
            import rejection.constants.alex_cluster_constants as constants
        else:
            import rejection.constants.cluster_constants as constants
    else:
        import rejection.constants.gpu_constants as constants

    config = load_config(args)[0]

    main(config)

